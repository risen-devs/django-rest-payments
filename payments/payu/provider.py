import json
import logging
from decimal import ROUND_HALF_UP, Decimal
from functools import wraps

import requests
from django.conf import settings
from django.utils import timezone

from .. import PaymentError, PaymentStatus
from ..core import BasicProvider

logger = logging.getLogger(__name__)


# http://developers.payu.com/pl/restapi.html#creating_new_order_api
CENTS = Decimal(1.)  # you need multiply by 100
HUF = Decimal(0)


def authorize(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        self = args[0]
        payment = args[1]
        self.access_token = self.get_access_token(payment)
        try:
            response = fun(*args, **kwargs)
        except requests.exceptions.HTTPError as e:
            if e.response.status_code == 401:
                last_auth_response = self.get_last_response(payment, is_auth=True)
                last_auth_response.pop('access_token', None)
                self.set_response_data(payment, last_auth_response, is_auth=True)
                self.access_token = self.get_access_token(payment)
                response = fun(*args, **kwargs)
            else:
                raise
        return response
    return wrapper


class PayUProvider(BasicProvider):
    '''
    This class defines the PayU provider API. It should not be instantiated
    directly. Use factory instead.
    '''
    _method = 'post'

    def __init__(self, client_id, secret, endpoint='https://secure.payu.com', **kwargs):
        self.secret = secret
        self.client_id = client_id
        self.endpoint = endpoint
        self.oauth2_url = self.endpoint + '/pl/standard/user/oauth/authorize'
        self.payments_url = self.endpoint + '/api/v2_1/orders'
        self.payment_self_url = self.payments_url + '/{orderId}'
        self.payment_execute_url = self.payments_url + '/{orderId}/status'
        self.payment_refund_url = (
            self.endpoint + '/v1/payments/capture/{captureId}/refund')
        super(PayUProvider, self).__init__(**kwargs)

    def get_action(self, payment):
        return self.get_return_url(payment)

    def get_hidden_fields(self, payment):
        '''
        Converts a payment into a dict containing transaction data. Use
        get_form instead to get a form suitable for templates.

        When implementing a new payment provider, overload this method to
        transfer provider-specific data.
        '''
        raise NotImplementedError()

    def process_data(self, payment, request):
        '''
        Process callback request from a payment provider.
        '''
        raise NotImplementedError()

    def get_token_from_request(self, payment, request):
        '''
        Return payment token from provider request.
        '''
        raise NotImplementedError()

    def _get_links(self, payment):
        extra_data = json.loads(payment.extra_data or '{}')
        links = extra_data.get('links', {})
        return links

    def get_access_token(self, payment):
        last_auth_response = self.get_last_response(payment, is_auth=True)
        created = payment.created
        now = timezone.now()
        if ('access_token' in last_auth_response and
                'expires_in' in last_auth_response and
                (created + timezone.timedelta(
                    seconds=last_auth_response['expires_in'])) > now):
            return '%s %s' % (last_auth_response['token_type'], last_auth_response['access_token'])
        else:
            headers = {'Accept': 'application/json', 'Accept-Language': 'en_US'}
            post = {'grant_type': 'client_credentials'}
            response = requests.post(self.oauth2_url, data=post, headers=headers, auth=(self.client_id, self.secret))
            response.raise_for_status()
            data = response.json()
            last_auth_response.update(data)
            self.set_response_data(payment, last_auth_response, is_auth=True)
            return '%s %s' % (data['token_type'], data['access_token'])

    def set_response_data(self, payment, response, is_auth=False):
        extra_data = json.loads(payment.extra_data or '{}')
        if is_auth:
            extra_data['auth_response'] = response
        else:
            extra_data['response'] = response
            if 'redirectUri' in response:
                extra_data['links'] = {
                    'approval_url': {
                        'href': response['redirectUri'],
                        'rel': 'approval_url',
                        'method': 'REDIRECT'
                    }
                }
        payment.extra_data = json.dumps(extra_data)

    def get_request_headers(self, payment):
        headers = {
            'Content-Type': 'application/json',
            'Authorization': self.access_token
        }
        return headers

    def post(self, payment, *args, **kwargs):
        kwargs['headers'] = self.get_request_headers(payment)
        kwargs['allow_redirects'] = False
        if 'data' in kwargs:
            kwargs['data'] = json.dumps(kwargs.get('data', None))
        response = requests.post(*args, **kwargs)
        try:
            data = response.json()
        except ValueError:
            data = {}
        if 400 <= response.status_code <= 500:
            if settings.DEBUG:
                print(data)
            self.set_error_data(payment, data)
            self.handle_provider_error(response, data, payment)
        else:
            self.set_response_data(payment, data)
        return data

    def put(self, payment, *args, **kwargs):
        kwargs['headers'] = self.get_request_headers(payment)
        if 'data' in kwargs:
            kwargs['data'] = json.dumps(kwargs.get('data', None))
        # TODO: update url with orderId
        response = requests.put(*args, **kwargs)
        try:
            data = response.json()
        except ValueError:
            data = {}
        if 400 <= response.status_code <= 500:
            self.set_error_data(payment, data)
            self.handle_provider_error(response, data)
        else:
            self.set_response_data(payment, data)
        return data

    def handle_provider_error(self, response, data, payment):
        logger.debug(data)
        message = 'PayU error'
        if response.status_code == 400:
            error_data = response.json()
            logger.warning(message, extra={
                'response': error_data,
                'status_code': response.status_code})
            message = error_data.get('message', message)
        else:
            logger.warning(
                message, extra={'status_code': response.status_code})
        payment.change_status(PaymentStatus.ERROR, message)
        raise PaymentError(message)

    @authorize
    def create_payment(self, payment, *args, **kwargs):
        extra_data = kwargs.get('extra_data', None)
        buyer = kwargs.get('buyer')
        # TODO - throw exception when no buyer
        payment_payload = self.get_payment_payload(payment, buyer, extra_data)
        payment_data = self.post(payment, self.payments_url, data=payment_payload)
        payment.transaction_id = payment_data['orderId']
        return payment

    def get_product_data(self, payment):
        return {}

    @authorize
    def get_payment(self, payment, *args, **kwargs):
        kwargs['headers'] = self.get_request_headers(payment)
        kwargs['allow_redirects'] = False
        kwargs['url'] = self.payment_self_url.format(orderId=payment.transaction_id)
        if 'data' in kwargs:
            kwargs['data'] = json.dumps(kwargs.get('data', None))
        response = requests.get(*args, **kwargs)
        try:
            data = response.json()
        except ValueError:
            data = {}
        if 400 <= response.status_code <= 500:
            self.set_error_data(payment, data)
            self.handle_provider_error(response, data, payment)
        else:
            self.set_response_data(payment, data)
        return data

    def execute_payment(self, payment, payload, *args, **kwargs):
        return self.payment_status(payment)

    def capture(self, payment, amount=None):
        raise NotImplementedError()

    def release(self, payment):
        raise NotImplementedError()

    def refund(self, payment, amount=None):
        raise NotImplementedError()

    @authorize
    def payment_status(self, payment, *args, **kwargs):
        provider_payment = self.get_payment(payment)
        status = provider_payment['orders'][0]['status']
        if status == 'NEW':
            payment.status = PaymentStatus.WAITING
        elif status == 'PENDING':
            payment.status = PaymentStatus.WAITING
        elif status == 'COMPLETED':
            payment.status = PaymentStatus.CONFIRMED
        elif status == 'REJECTED':
            payment.status = PaymentStatus.REJECTED
        else:
            payment.status = PaymentStatus.ERROR
        payment.save(update_fields=['status'])
        return payment.status

    def get_payment_payload(self, payment, buyer, *args, **kwargs):
        total = (payment.total * Decimal(100)).quantize(CENTS, rounding=ROUND_HALF_UP)
        buyer = self._get_buyer(buyer)
        description = kwargs.get('description', '')
        payload = {
            "notifyUrl": "https://your.eshop.com/notify",
            "continueUrl": self.get_return_url(payment),
            "customerIp": "127.0.0.1",
            "merchantPosId": self.client_id,
            "description": description,
            "currencyCode": payment.currency,
            "totalAmount": str(total),
            "buyer": buyer,
            "settings": {
                "invoiceDisabled": "true"
            },
            "products": self.get_payment_items(payment)
        }
        return payload

    def get_payment_items(self, payment):
        items = []
        for item in payment.get_purchased_items():
            items.append(
                {
                    "name": item.name,
                    "unitPrice": str((item.price * 100).quantize(CENTS, rounding=ROUND_HALF_UP)),
                    "quantity": item.quantity,
                }
            )
        return items

    # TODO Move it from here
    def _get_buyer(self, user):
        # TODO, remove this hack

        return {
            'email': 'test@test.pl',
            'firstName': 'first_name',
            'lastName': 'last_name'
        }
        # data = {
        #     "email": user.email,
        #     # 'firstName': buyer.first_name,
        #     # 'lastName': buyer.last_name
        # }
        # return data
