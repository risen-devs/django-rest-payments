from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from .fields import (CreditCardExpiryField, CreditCardNameField,
                     CreditCardNumberField, CreditCardVerificationField)

try:
    from collections import OrderedDict
except ImportError:
    from django.utils.datastructures import SortedDict as OrderedDict




class PaymentSerializer(serializers.Serializer):
    '''
    Payment base serializer
    '''
    def __init__(self, data=None, action='', method='post', provider=None,
                 payment=None, hidden_inputs=True, autosubmit=False):
        # if hidden_inputs and data is not None:
        #     super(PaymentSerializer, self).__init__(auto_id=False)
        #     for key, val in data.items():
        #         widget = forms.widgets.HiddenInput()
        #         self.fields[key] = forms.CharField(initial=val, widget=widget)
        # else:
        super(PaymentSerializer, self).__init__(data=data)
        self.action = action
        self.autosubmit = autosubmit
        self.method = method
        self.provider = provider
        self.payment = payment


class CreditCardPaymentSerializer(PaymentSerializer):

    number = CreditCardNumberField(label=_('Card Number'), max_length=32,
                                   required=True)
    # expiration = CreditCardExpiryField()
    expiration = serializers.DateTimeField()
    cvv2 = CreditCardVerificationField(required=False)
        # label=_('CVV2 Security Number'), required=False, help_text=_(
        #     'Last three digits located on the back of your card.'
        #     ' For American Express the four digits found on the front side.'))

    # def __init__(self, *args, **kwargs):
    #     super(CreditCardPaymentSerializer, self).__init__(
    #         hidden_inputs=False, *args, **kwargs)
    #     if hasattr(self, 'VALID_TYPES'):
    #         self.fields['number'].valid_types = self.VALID_TYPES


class CreditCardPaymentSerializerWithName(CreditCardPaymentSerializer):

    name = CreditCardNameField(label=_('Name on Credit Card'), max_length=128)

    def __init__(self, *args, **kwargs):
        super(CreditCardPaymentSerializerWithName, self).__init__(*args, **kwargs)
        name_field = self.fields.pop('name')
        fields = OrderedDict({'name': name_field})
        fields.update(self.fields)
        self.fields = fields
